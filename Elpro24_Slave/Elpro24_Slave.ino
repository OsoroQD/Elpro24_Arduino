#include <avr/wdt.h>
#include <MemoryFree.h>
#include <pgmStrToRAM.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>
#include "timer-api.h"

#define DEBUG 1

//program reset
void(* reset) (void) = 0;

//set static IP address if DHCP failed
IPAddress ip(192, 168, 0, 177);
//set MAC
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//set domen name
char server[] = "www.osora.ru"; 

//set requests
char auth[] = "GET /elpro24/API/getDataStripped?code=arduinoTest HTTP/1.1";
char set[] = "POST /elpro24/API/setDataStripped HTTP/1.1";
char getTime[] = "GET /elpro24/API/getTime HTTP/1.1";
boolean isSync = false;

EthernetClient client;
SoftwareSerial soft(5, 3);

int dataLength = 0;

void setup() {
  Serial.begin(9600);
  soft.begin(9600);
  //pinMode(13, OUTPUT);
  wdt_enable(WDTO_8S);
  
  //wait for DHCP...
  getConnection();

  #if DEBUG
  Serial.write('r');
  #endif

  soft.write("r");
}

void loop() {
  wdt_reset();
  if (isSync){
    soft.write("r");
    Serial.println(F("r"));
    isSync = false;
    return;
  }
  //waiting for command from Master...
  while(!soft.available()){return;}

  char c = soft.read();

  switch(c){
    //getTime command
    case 't':{
      #if DEBUG
      Serial.println(F("t"));
      #endif
      getRequest(getTime);
      waitConnection();
      gotoBody();

      char currentTime[15];
      currentTime[0] = 't';
      for (short i = 1; i < 14; i++){
        char t = client.read();
        Serial.print(t);
        currentTime[i] = t;
      }
      Serial.println(); Serial.println();
      client.stop();
      currentTime[14] = 0;
      soft.println(currentTime);
      Serial.println(currentTime);   
      break;
    }
    //ready command
    case 'r':{
      soft.write(F("r"));
      break;
    }
    //get skips command
    case 's':{
      timer_stop_ISR(TIMER_DEFAULT);
      #if DEBUG
      Serial.println(F("s"));
      #endif

      getRequest(auth);
      waitConnection();

      int dataLength = getDataLength(8);
      if (dataLength < 1)
        reset();
      gotoBody();
      
      if (!checkDataLength(dataLength))
        break;
      
      parseAndSendData();
      break;
    }
    //set passages command
    case 'p':{    
      #if DEBUG
      Serial.println(F("p"));  
      #endif

      timer_stop_ISR(TIMER_DEFAULT);
      String str = "code=arduinoTest&data=";
      str += soft.readString();
      str.replace("\n", "");
      
      int n = str.length() + 1;
      char* request = new char[n];
      str.toCharArray(request, n);

      Serial.println(request);
      
      do{       
        #if DEBUG
        Serial.println(F("do")); 
        Serial.println(freeMemory());
        #endif

        postRequest(set, request);
        
        waitConnection();
        #if DEBUF
        Serial.println("w");
        #endif
      }
      while(!getPostResponse());

      //send OK to soft
      soft.print("d");

      #if DEBUG
      Serial.println('d');
      #endif

      timer_init_ISR_1Hz(TIMER_DEFAULT);
      delete[] request;
      break;
    }
  }
}

//check setData response
boolean getPostResponse(){
  gotoBody();
  char c = client.read();

  #if DEBUG
  Serial.println(c);
  #endif

  client.stop();
  return c == 't';
}

boolean checkDataLength(int len){
  int lenlen = getCountLength(len);
  //Serial.println(lenlen);
  char temp[lenlen];
  int i = 0;
  
  while(true){
    char c = client.read();
    if(c == '#')
      break;
    temp[i++] = c;   
  }
  
  dataLength = atoi(temp);
  return dataLength == len;
}

//parse inputs from server and send it to Master
void parseAndSendData(){
  boolean isEnd = false;
  while(!isEnd){
    char buf[115];
    buf[0] = 's';
    for (int d = 1; d < 113; d++){
      char c = client.read();
      //Serial.println(c);
      if (c == ';'){
        buf[d] = ';';
        isEnd = true;
        break;
      }
      buf[d] = c;
    }
    buf[113] = ';';
    buf[114] = 0;
    
    soft.println(buf);
    Serial.println(buf);
    
    while (!soft.available()){}

    char stat = soft.read();
    Serial.println(stat);
    if (stat != 'g')
      break;  
  }
  client.stop();
  soft.write("e");
  Serial.println("e");
  timer_init_ISR_1Hz(TIMER_DEFAULT);
}

//wait for DHCP
void getConnection(){
  int i = 0;
  while(Ethernet.begin(mac) == 0) {
    i++;
    Serial.println(F("false"));
    wdt_reset();
    if (i == 5)
      delay(8000);
      return;
    delay(2000);
  }
}

//send GET request
void getRequest(char* requestBody){
  int i = 0;
  while (!client.connect(server, 80)) {
    i++;
    delay(200);
    if (i == 600)
      delay(8000);
    wdt_reset();
  }

  Serial.println(F("connect"));

  client.println(requestBody);
  client.println(F("Host: www.osora.ru"));
  client.println(F("Connection: close"));
  client.println();
}

//send POST request
void postRequest(char* requestBody, char* data){
  int i = 0;
  while (!client.connect(server, 80)) {
    i++;
    delay(200);
    if (i == 200)
      delay(8000);
    wdt_reset();
  }

  int len = strlen(data) - 1;

  Serial.println(F("Connect"));
  
  client.println(requestBody);
  client.println(F("Host: www.osora.ru"));
  client.println(F("Accept: */*"));
  client.println("User-Agent: arduino-ethernet");
  client.println(F("Connection: close"));
  client.print(F("Content-Length: "));
  client.println(len);
  client.println(F("Content-Type: application/x-www-form-urlencoded"));
  client.print("\r\n");
  //client.print(F("data="));
  client.println(data);
  client.println();

  #if DEBUG
  Serial.println(requestBody);
  Serial.println(F("Host: www.osora.ru"));
  Serial.println("User-Agent: arduino-ethernet");
  Serial.println(F("Connection: close"));
  Serial.print(F("Content-Length: "));
  Serial.println(len);
  Serial.print("\r\n");
  //Serial.print(F("data="));
  Serial.println(data);
  Serial.println();
  #endif
}

//skip headers
void gotoBody(){
  boolean currentLineIsBlank = true, isValue = false;
  short i = 0;
  while(!isValue){
    char c = client.read();
  
    if (c == '\n' && currentLineIsBlank){
      isValue = true;
      continue;
    }
    if (c == '\n')
      currentLineIsBlank = true;
    else if (c != '\r')
      currentLineIsBlank = false;
    i++;
  }
}

//wait for server response
void waitConnection(){
  int i = 0;
  while(client.connected() && !client.available() && i < 1000) {
    wdt_reset();
    delay(100);
    i++;
    if (i == 200){
      Serial.println(F("reset"));
      delay(8000);
    }
  }
}

//parse header and get input data length
int getDataLength(int lengthIndex){
  String temp;
  int i = 0;
  char lf = 10;
  while (client.connected() || client.available()) {
    char c = client.read();
    if (c == lf) 
      i++;
    else if (i == lengthIndex){
      if (isDigit(c)){
        temp += c;
      }
        
    }
    else if (i > lengthIndex){
      return temp.toInt();
    }
  }
  return -1;
}

int getCountLength(int count){
  if(count > 9999)
    return 5;
  else if(count > 999)
    return 4;
  else if(count > 99)
    return 3;
  else if(count > 9)
    return 2;
  else
    return 1;
}

//timer function
void timer_handle_interrupts(int timer){
  //delay = count * interval
  //interval = 1000ms
  //delay = 300s = 5min
  static int count = 30;

  if (count == 0){
    #if DEBUG
    Serial.println(F("timer"));
    #endif

    isSync = true;
    //client.stop();
    //soft.write("r");
    
    count = 30;
  }
  else
    count--;  
}


