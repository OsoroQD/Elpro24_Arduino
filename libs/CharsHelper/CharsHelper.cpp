#include "Arduino.h"
#include "CharsHelper.h"

void CharsHelper::strTrim(char *chars) {
	int j = 0;
	while (chars[j] == ' ')
		j++;

	int i;
	if (j != 0)
		for (i = -1; chars[j] != 0; j++)
			chars[++i] = chars[j];
	else
		i = strlen(chars) - 1;

	while (chars[i] == ' ')
		i--;
	chars[i + 1] = 0;
}

void CharsHelper::strReplace(char *chars, char oldChar, char newChar) {
    int n = strlen(chars);
    for (int i = 0; i < n; i++)
        if (chars[i] == oldChar)
            chars[i] = newChar;
}

void CharsHelper::strRemove(char *chars, char removeChar) {
	int n = strlen(chars), i = 0;
	for (int j = 0; j < n;) {
		if (chars[j] == removeChar)
			j++;
		else {
			if (i != j)
				chars[i] = chars[j];
			i++; j++;
		}
	}
	chars[i] = 0;
}

char* CharsHelper::strConcat(char* firstChars, char* secondChars) {
	bool firstCharsIsNull = firstChars == NULL, secondCharsIsNull = secondChars == NULL;
	int firstCharsLength = firstCharsIsNull ? 0 : strlen(firstChars), totalLength = firstCharsLength + (secondCharsIsNull ? 0 : strlen(secondChars)) + 1;
	char* chars = new char[totalLength];

	if (firstCharsIsNull && secondCharsIsNull)
		chars[0] = 0;
	else {
		if (!firstCharsIsNull)
			strcpy(chars, firstChars);
		if (!secondCharsIsNull)
			strcpy(chars + firstCharsLength, secondChars);
	}

	return chars;
}

char* CharsHelper::strSubstring(char* str, short x, short y){
    char* ret = new char[y + 1];

    for (short i = x; i < x + y; i++)
        ret[i - x] = str[i];

    ret[y] = '\0';
    return ret;
}

char* CharsHelper::toDigits(char* str){
  int i = 0;
  while(str[i] == '0') i++;
  int len = strlen(str) + 1;
  char* result = new char[len - i];
  for (int j = 0; j < len - i; j++){
    result[j] = str[i + j];
  }
  result[len - i - 1] = 0;
  return result;
}