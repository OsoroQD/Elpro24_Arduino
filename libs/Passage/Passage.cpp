#include "Arduino.h"
#include "Passage.h"

Passage::Passage() : Passage(NULL, NULL) {
}

Passage::Passage(char* skip_id) : Passage(skip_id, NULL) {
}

Passage::Passage(char* skip_id, char* date) {
    _skip_id = skip_id;
    _date = date;
}

Passage::~Passage() {
    if (_skip_id != NULL)
        delete[] _skip_id;
    if (_date != NULL)
        delete[] _date;
}

char* Passage::getId(){
    return _skip_id;
}

char* Passage::getDate(){
    return _date;
}

void Passage::setId(char* skip_id){
    _skip_id = skip_id;
}

void Passage::setDate(char* date){
    _date = date;
}


