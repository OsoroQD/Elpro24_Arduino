#ifndef Passage_h
#define Passage_h

#pragma once

#include "Arduino.h"

class Passage{
    public:
        Passage();
        Passage(char* skip_id);
        Passage(char* skip_id, char* date);  
        ~Passage();      
        char* getId(); 
        char* getDate();
        void setId(char* skip_id);
        void setDate(char* date);

    private:
        char* _skip_id;
        char* _date;

};


#endif
