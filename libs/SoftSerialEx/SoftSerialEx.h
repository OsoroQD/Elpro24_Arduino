#ifndef SoftSerialEx_h
#define SoftSerialEx_h

#pragma once

#include "Arduino.h"
#include <SoftwareSerial.h>
#include <CharsHelper.h>
#include "Worker.h"
#include "CharsHelper.h"
#include "Database.h"


class SoftSerialEx{
    private:
    

    public:
        SoftwareSerial soft = SoftwareSerial(8,9);  
        CharsHelper ch;
        SoftSerialEx();
        char* getSerialData(int &dataLength);
        Worker** parseAndCreateSkips(char* buf, int skipLength, int skipsCount);
        boolean parseAndCreateSkips(char* buf, int skipLength, int skipsCount, Database* db);
        int available();
        void send(char* data);
        void println(char* data);
        int read();
};

#endif