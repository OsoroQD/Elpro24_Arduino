#include "Arduino.h"
#include "SoftSerialEx.h"

#define DEBUG 0

#if DEBUG
#include <MemoryFree.h>
#include <pgmStrToRAM.h>
#endif

SoftSerialEx::SoftSerialEx(){
    soft.begin(9600);
}

char* SoftSerialEx::getSerialData(int &dataLength){
    dataLength = soft.available();
    char* buf = new char[dataLength];

    #if DEBUG
    Serial.println(dataLength);
    #endif

    for(int i = 0; i < dataLength; i++) {
        char c = soft.read();
        buf[i] = c;
    }

    #if DEBUG
    Serial.println(buf);
    #endif
    
    return buf;
}

Worker** SoftSerialEx::parseAndCreateSkips(char* buf, int skipLength, int skipsCount){
    boolean isEnd = false;
    Worker** workers = new Worker*[skipsCount];
    #if DEBUG
    Serial.print(__LINE__);
    Serial.println(freeMemory());
    #endif

    for(int i = 0; i < skipsCount; i++){
        #if DEBUG
        Serial.print(__LINE__);
        Serial.println(freeMemory());
        #endif
        if (isEnd) break;
        
        char* data = new char[15];
        for(int j = 0; j < skipLength; j++){
            if (buf[i * skipLength + j] == ';'){
                data[j] = 0;
                isEnd = true;
                break;
            }
            data[j] = buf[i * 14 + j];
        }
        data[14] = 0;
        
        workers[i] = new Worker(ch.strSubstring(data, 0, 8), ch.strSubstring(data, 8, 6));
        delete[] data;
        data = nullptr;
        #if DEBUG
        Serial.print(__LINE__);
        Serial.println(freeMemory());
        #endif
    }
    delete[] buf;
    buf = nullptr;
    return workers;
}

boolean SoftSerialEx::parseAndCreateSkips(char* buf, int skipLength, int skipsCount, Database* db){
    boolean isEnd = false;
    
    #if DEBUG
    Serial.println(freeMemory());
    #endif

    for(int i = 0; i < skipsCount; i++){
        if (isEnd) break;
        
        char* data = new char[15];
        for(int j = 0; j < skipLength; j++){
            if (buf[i * skipLength + j] == ';'){
                data[j] = 0;
                isEnd = true;
                break;
            }
            data[j] = buf[i * 14 + j];
        }
        data[14] = 0;
        if (isEnd) {
            delete[] data;
            break;
        }

        #if DEBUG
        Serial.println(ch.strSubstring(data, 0, 8));
        Serial.println(ch.strSubstring(data, 8, 6));
        #endif

        db->AddSkip(ch.strSubstring(data, 0, 8), ch.strSubstring(data, 8, 6));  
        delete[] data;
        data = nullptr;
        
    }
    delete[] buf;
    buf = nullptr;
    return true;
}

int SoftSerialEx::available(){
    return soft.available();
}

void SoftSerialEx::send(char* data){
    soft.write(data);
}

void SoftSerialEx::println(char* data){
    soft.println(data);
}

int SoftSerialEx::read(){
    return soft.read();
}